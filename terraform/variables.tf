# Need to upgrade to new model
locals {
  env            = terraform.workspace
  region         = data.terraform_remote_state._42.outputs.region
  aws_account_id = data.terraform_remote_state._42.outputs.aws_account
}

output "region" { value  = data.terraform_remote_state._42.outputs.region }

#Tags:
variable "tags" {
  default = {
    terraform  = "true"
    team       = "meiosdepagamento"
    subteam    = "squadz"
    project    = "vpc"
    repository = "git@bitbucket.org:git-m4u/meiosdepagamento-pci-squadz-vpc.git"
  }
}
 
# Routes to tgw:
variable "plataformas_squadz_cidr" {
  default = { dev = "10.22.0.0/20"
              stg = "10.22.0.0/20"
              prd = "10.17.0.0/16" }
}

variable "team"{ default = "meiosdepagamento" }
variable "subteam"{ default = "squadz" }
variable "budget_limit" { default = { dev = "200", stg = "200", prd = "1000" } }
variable "email" { default = "dev_mp_squadz@m4u.com.br" }