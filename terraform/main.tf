# Terraform:
provider "aws" {
  region = local.region
  #  default_tags { tags = local.provider_tags }
  assume_role { role_arn = "arn:aws:iam::${local.aws_account_id}:role/deployer-role" }
}

terraform { 
  required_version = ">= 0.13"
  required_providers {
    aws = { version = ">= 3.10.0" }
  }
  backend "s3" {
    bucket    = "m4u-meiosdepagamento-pci-terraform-states"
    region    = "sa-east-1"
    key       = "squadz/vpc.state"
    role_arn  = "arn:aws:iam::816705007546:role/deployer-role"
    acl       = "bucket-owner-full-control"
  }
}


# Deep thought remote state:
data "terraform_remote_state" "_42" {
  backend   = "s3"
  workspace = terraform.workspace
  config = {
    bucket   = "m4u-meiosdepagamento-pci-terraform-states"
    region   = "sa-east-1"
    key      = "squadz/deep_thought.state"
    role_arn = "arn:aws:iam::816705007546:role/deployer-role"
  }
}

# Transit gateway:
data "terraform_remote_state" "tgw" {
  backend   = "s3"
  workspace = terraform.workspace
  config = {
    bucket   = "m4u-plataformas-pci-terraform-states"
    region   = "sa-east-1"
    key      = "common/tgw.state"
    role_arn = "arn:aws:iam::816705007546:role/deployer-role"
  }
}




