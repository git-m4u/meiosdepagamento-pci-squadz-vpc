resource "aws_ec2_transit_gateway_vpc_attachment" "tgw_attach" {
  subnet_ids         = aws_subnet.public.*.id
  transit_gateway_id = data.terraform_remote_state.tgw.outputs.tgw_id 
  vpc_id             = aws_vpc.vpc.id
  tags = merge(var.tags, {
    Name           = "${data.terraform_remote_state._42.outputs.subteam}-vpc-tgw-attach"
    environment    = terraform.workspace
  },)
}

resource "aws_route" "plataformas_pci_cidr" {
  count                  = data.terraform_remote_state._42.outputs.azcount
  route_table_id         = element(aws_route_table.ngw_rt.*.id, count.index)
  destination_cidr_block = data.terraform_remote_state._42.outputs.plataformas_pci_cidr
  transit_gateway_id     = data.terraform_remote_state.tgw.outputs.tgw_id
}

resource "aws_route" "m4u_incore_pci_cidr" {
  count                  = data.terraform_remote_state._42.outputs.azcount
  route_table_id         = element(aws_route_table.ngw_rt.*.id, count.index)
  destination_cidr_block = data.terraform_remote_state._42.outputs.m4u_incore_pci_cidr
  transit_gateway_id     = data.terraform_remote_state.tgw.outputs.tgw_id
}

resource "aws_route" "m4u_outcore_pci_cidr" {
  count                  = data.terraform_remote_state._42.outputs.azcount
  route_table_id         = element(aws_route_table.ngw_rt.*.id, count.index)
  destination_cidr_block = data.terraform_remote_state._42.outputs.m4u_outcore_pci_cidr
  transit_gateway_id     = data.terraform_remote_state.tgw.outputs.tgw_id
}
