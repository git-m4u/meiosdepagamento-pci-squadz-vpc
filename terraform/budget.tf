resource "aws_budgets_budget" "budget" {
  name              = "${var.team}-${var.subteam}"
  budget_type       = "COST"
  limit_amount      = var.budget_limit[terraform.workspace]
  limit_unit        = "USD"
#  time_period_end   = "2087-06-15_00:00"
  time_period_start = "2017-07-01_00:00"
  time_unit         = "MONTHLY"

#  cost_filters = {
#    TagKeyValue = join("", [ "user:subteam$", var.subteam ] )
#  }

  notification {
    comparison_operator        = "GREATER_THAN"
    threshold                  = 80
    threshold_type             = "PERCENTAGE"
    notification_type          = "FORECASTED"
    subscriber_email_addresses = [ var.email ]
  }
}

