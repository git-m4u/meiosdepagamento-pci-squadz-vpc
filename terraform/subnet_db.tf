## Public subnets, one for each AZ:
resource "aws_subnet" "db" {
  count  = data.terraform_remote_state._42.outputs.azcount
  vpc_id = aws_vpc.vpc.id
  cidr_block = element(data.terraform_remote_state._42.outputs.db_subnet_cidr,count.index)
  map_public_ip_on_launch = false
  availability_zone = element(data.terraform_remote_state._42.outputs.azs, count.index)
  tags = merge(var.tags, {
    Name           = "${data.terraform_remote_state._42.outputs.subteam}-vpc-db-${element(data.terraform_remote_state._42.outputs.azs, count.index)}"
    environment    = terraform.workspace
  },)
}
output "db_subnet_id" { value = aws_subnet.db.*.id }


resource "aws_route_table_association" "db_rta" {
  count  = data.terraform_remote_state._42.outputs.azcount
  subnet_id      = element(aws_subnet.db.*.id, count.index)
  route_table_id = element(aws_route_table.ngw_rt.*.id, count.index)
}


