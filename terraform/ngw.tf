# The NAT gateways
resource "aws_nat_gateway" "ngw" {
  count         = data.terraform_remote_state._42.outputs.azcount
  allocation_id = element(aws_eip.ngw_eip.*.id, count.index)
  subnet_id     = element(aws_subnet.public.*.id, count.index)
  tags = merge(var.tags, {
    Name           = "${data.terraform_remote_state._42.outputs.subteam}-vpc-ngw-${element(data.terraform_remote_state._42.outputs.azs, count.index)}"
    environment    = terraform.workspace
  },)
}
output "ngw_id" { value = aws_nat_gateway.ngw.*.id }

# Elastic IPs:
resource "aws_eip" "ngw_eip" {
  count = data.terraform_remote_state._42.outputs.azcount
  vpc   = true
  tags = merge(var.tags, {
    Name           = "${data.terraform_remote_state._42.outputs.subteam}-vpc-ngw-eip-${element(data.terraform_remote_state._42.outputs.azs, count.index)}"
    environment    = terraform.workspace
  },)
}

output "ngw_eip_public_ip" { value = aws_eip.ngw_eip.*.public_ip }



# Route table to nat gateways
resource "aws_route_table" "ngw_rt" {
  count  = data.terraform_remote_state._42.outputs.azcount
  vpc_id = aws_vpc.vpc.id
  tags = merge(var.tags, {
    Name           = "${data.terraform_remote_state._42.outputs.subteam}-vpc-ngw-rt-${element(data.terraform_remote_state._42.outputs.azs, count.index)}"
    environment    = terraform.workspace
  },)
}
output "ngw_rt_id" { value = aws_route_table.ngw_rt.*.id }

# Default route:
resource "aws_route" "default_route" {
  count = data.terraform_remote_state._42.outputs.azcount
  destination_cidr_block = "0.0.0.0/0"
  route_table_id = element(aws_route_table.ngw_rt.*.id, count.index)
  nat_gateway_id = element(aws_nat_gateway.ngw.*.id, count.index)
}


# To isquadzs on plataformas
resource "aws_route" "squadz" {
  count = data.terraform_remote_state._42.outputs.azcount
  destination_cidr_block = var.plataformas_squadz_cidr[terraform.workspace]
  route_table_id = element(aws_route_table.ngw_rt.*.id, count.index)
  transit_gateway_id = data.terraform_remote_state.tgw.outputs.tgw_id
}

